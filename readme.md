
## Keen Seaweed Cieu

A kanban project. Primarily as a sketch for interacting with a database with a handrolled data layer. Eventually (secondarily), a sketch for updating a swing ui with websockets. Cieu is 9 using the latin alphabet for the Shanghainese (Chinese) word.

### license

Keen Seaweed Cieu is &copy; Nicholas Prado, but released under ISC v2.0 license terms.
