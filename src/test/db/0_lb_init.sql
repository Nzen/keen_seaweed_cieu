Starting Liquibase at Tue, 04 Sep 2018 22:24:14 CDT (version 3.6.2 built at 2018-07-03 11:28:09)
-- *********************************************************************
-- Update Database Script
-- *********************************************************************
-- Change Log: src/main/db/0_init.yaml
-- Ran at: 9/4/18 10:24 PM
-- Against: FIRST_USER@jdbc:h2:file:~/workspace/java_photon/keen_seaweed_cieu/usr/ksc_db
-- Liquibase version: 3.6.2
-- *********************************************************************

-- Create Database Lock Table
CREATE TABLE PUBLIC.DATABASECHANGELOGLOCK (ID INT NOT NULL, LOCKED BOOLEAN NOT NULL, LOCKGRANTED TIMESTAMP, LOCKEDBY VARCHAR(255), CONSTRAINT PK_DATABASECHANGELOGLOCK PRIMARY KEY (ID));

-- Initialize Database Lock Table
DELETE FROM PUBLIC.DATABASECHANGELOGLOCK;

INSERT INTO PUBLIC.DATABASECHANGELOGLOCK (ID, LOCKED) VALUES (1, FALSE);

-- Lock Database
UPDATE PUBLIC.DATABASECHANGELOGLOCK SET LOCKED = TRUE, LOCKEDBY = '10.0.0.4 (10.0.0.4)', LOCKGRANTED = '2018-09-04 22:24:15.696' WHERE ID = 1 AND LOCKED = FALSE;

-- Create Database Change Log Table
CREATE TABLE PUBLIC.DATABASECHANGELOG (ID VARCHAR(255) NOT NULL, AUTHOR VARCHAR(255) NOT NULL, FILENAME VARCHAR(255) NOT NULL, DATEEXECUTED TIMESTAMP NOT NULL, ORDEREXECUTED INT NOT NULL, EXECTYPE VARCHAR(10) NOT NULL, MD5SUM VARCHAR(35), DESCRIPTION VARCHAR(255), COMMENTS VARCHAR(255), TAG VARCHAR(255), LIQUIBASE VARCHAR(20), CONTEXTS VARCHAR(255), LABELS VARCHAR(255), DEPLOYMENT_ID VARCHAR(10));

-- Changeset src/main/db/0_init.yaml::0_initial_tables::nzen
CREATE TABLE PUBLIC.base_user (id INT AUTO_INCREMENT NOT NULL, desc VARCHAR(90), when_created date DEFAULT CURRENT_DATE(), password_hash VARCHAR(256), CONSTRAINT PK_BASE_USER PRIMARY KEY (id));

CREATE TABLE PUBLIC.base_board (id INT AUTO_INCREMENT NOT NULL, desc VARCHAR(90), priority INT, when_created date DEFAULT CURRENT_DATE(), shared BOOLEAN DEFAULT FALSE, CONSTRAINT PK_BASE_BOARD PRIMARY KEY (id));

CREATE TABLE PUBLIC.base_lane (id INT AUTO_INCREMENT NOT NULL, board INT, desc VARCHAR(90), priority INT, when_created date DEFAULT CURRENT_DATE(), hidden BOOLEAN DEFAULT FALSE, CONSTRAINT PK_BASE_LANE PRIMARY KEY (id), CONSTRAINT bl_fk_board FOREIGN KEY (board) REFERENCES PUBLIC.base_board( id ));

CREATE TABLE PUBLIC.base_task (id INT AUTO_INCREMENT NOT NULL, lane INT, user INT, desc VARCHAR(90), priority INT, when_created TIMESTAMP DEFAULT NOW(), created_utc_offset CHAR(6), CONSTRAINT PK_BASE_TASK PRIMARY KEY (id), CONSTRAINT bt_fk_user FOREIGN KEY (user) REFERENCES PUBLIC.base_user( id ), CONSTRAINT bt_fk_lane FOREIGN KEY (lane) REFERENCES PUBLIC.base_lane( id ));

CREATE TABLE PUBLIC.base_note (id INT AUTO_INCREMENT NOT NULL, task INT, user INT, desc CLOB, when_created TIMESTAMP DEFAULT NOW(), created_utc_offset CHAR(6), when_last_edited TIMESTAMP, edited_utc_offset CHAR(6), CONSTRAINT PK_BASE_NOTE PRIMARY KEY (id), CONSTRAINT bn_fk_user FOREIGN KEY (user) REFERENCES PUBLIC.base_user( id ), CONSTRAINT bn_fk_task FOREIGN KEY (task) REFERENCES PUBLIC.base_task( id ));

INSERT INTO PUBLIC.DATABASECHANGELOG (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('0_initial_tables', 'nzen', 'src/main/db/0_init.yaml', NOW(), 1, '8:725c79aea5857f4fb3aa1d4b2e26a646', 'createTable tableName=base_user; createTable tableName=base_board; createTable tableName=base_lane; createTable tableName=base_task; createTable tableName=base_note', '', 'EXECUTED', NULL, NULL, '3.6.2', '6117856539');

-- Release Database Lock
UPDATE PUBLIC.DATABASECHANGELOGLOCK SET LOCKED = FALSE, LOCKEDBY = NULL, LOCKGRANTED = NULL WHERE ID = 1;

