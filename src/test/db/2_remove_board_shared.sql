Starting Liquibase at Sat, 29 Sep 2018 17:26:54 CDT (version 3.6.2 built at 2018-07-03 11:28:09)
-- *********************************************************************
-- Update Database Script
-- *********************************************************************
-- Change Log: src/main/db/2_remove_fields.yaml
-- Ran at: 9/29/18 5:26 PM
-- Against: FIRST_USER@jdbc:h2:file:~/workspace/java_photon/keen_seaweed_cieu/usr/ksc_db
-- Liquibase version: 3.6.2
-- *********************************************************************

-- Lock Database
UPDATE PUBLIC.DATABASECHANGELOGLOCK SET LOCKED = TRUE, LOCKEDBY = '10.0.0.2 (10.0.0.2)', LOCKGRANTED = '2018-09-29 17:26:55.152' WHERE ID = 1 AND LOCKED = FALSE;

-- Changeset src/main/db/2_remove_fields.yaml::2_remove_chaff::nzen
ALTER TABLE PUBLIC.base_board DROP COLUMN shared;

INSERT INTO PUBLIC.DATABASECHANGELOG (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('2_remove_chaff', 'nzen', 'src/main/db/2_remove_fields.yaml', NOW(), 2, '8:2dff9c42e18c6c4bc542b97b59446f57', 'dropColumn columnName=shared, tableName=base_board', '', 'EXECUTED', NULL, NULL, '3.6.2', '8260016183');

-- Release Database Lock
UPDATE PUBLIC.DATABASECHANGELOGLOCK SET LOCKED = FALSE, LOCKEDBY = NULL, LOCKGRANTED = NULL WHERE ID = 1;

Liquibase command 'updateSql' was executed successfully.
