package keen_seaweed_cieu;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ws.nzen.schedule.ksc.model.KscTable;
import ws.nzen.schedule.ksc.model.TableType;

/** temporarily serving as a database and dao in one. Good thing this is just a sketch. */
public class RamStore
{

	public static RamStore singleton = new RamStore();
	private static Map<TableType, List<KscTable>> notDb = new HashMap<>();


	public boolean createIfMissing( TableType which )
	{
		if ( ! notDb.containsKey( which ) || notDb.get( which ) == null )
		{
			notDb.put( which, new LinkedList<>() );
		}
		return true;
	}


	/** your untyped record or null */
	public KscTable ofId( TableType which, int id )
	{
		if ( ! notDb.containsKey( which ) )
		{
			createIfMissing( which );
			return null;
		}
		for ( KscTable currRecord : notDb.get( which ) )
		{
			if ( currRecord.getId() == id )
			{
				return currRecord;
			}
		}
		return null;
	}


	public int countOf( TableType which )
	{
		createIfMissing( which );
		return notDb.get( which ).size();
	}


	public boolean save( TableType which, KscTable updated )
	{
		final boolean worked = true;
		createIfMissing( which );
		for ( KscTable currRecord : notDb.get( which ) )
		{
			if ( currRecord.getId() == updated.getId() )
			{
				currRecord = updated; // ASK weak reference ?
				return worked;
			}
		}
		notDb.get( which ).add( updated );
		return worked;
	}


	public List<Integer> allIdsOf( TableType which )
	{
		List<Integer> idsOf = new LinkedList<>();
		createIfMissing( which );
		for ( KscTable currRecord : notDb.get( which ) )
		{
			idsOf.add( currRecord.getId() );
		}
		return idsOf;
	}

}
