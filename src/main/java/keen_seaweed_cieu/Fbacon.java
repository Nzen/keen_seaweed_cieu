/**
 * 
 */
package keen_seaweed_cieu;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.TreeMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import ws.nzen.schedule.ksc.dao.ExceptionFallback;
import ws.nzen.schedule.ksc.dao.H2Board;
import ws.nzen.schedule.ksc.dao.H2Lane;
import ws.nzen.schedule.ksc.dao.Pool;
import ws.nzen.schedule.ksc.io.Controller;
import ws.nzen.schedule.ksc.io.Message;
import ws.nzen.schedule.ksc.io.MessageType;
import ws.nzen.schedule.ksc.model.Board;
import ws.nzen.schedule.ksc.model.Lane;
import ws.nzen.schedule.ksc.model.TableType;

/** @author nzen */
public class Fbacon
{
	private static final String cl = "fb.";
	final String dbInfoFilePath = "etc/db.properties";


	/** @param args
	 */
	public static void main( String[] args )
	{
		Fbacon experiment = new Fbacon();
		// experiment.sketchDaoConnection();
		// experiment.sketchDirectConnect();
		// experiment.sketchRamInteraction();
		experiment.sketchBoardCreation();
	}


	public void sketchDirectConnect()
	{
		String here = cl + "sdc ";
		Properties dbInfo = getConfigFrom( dbInfoFilePath );
		try (java.sql.Connection pipe = java.sql.DriverManager
				.getConnection( dbInfo.getProperty( Pool.propKeyUrl ),
						dbInfo.getProperty( Pool.propKeyUser ),
						dbInfo.getProperty( Pool.propKeyPassword ) ))
		{
			String getHardcodedLiteral = "SELECT 1;";
			java.sql.Statement incantation = pipe.createStatement();
			java.sql.ResultSet answerBag = incantation.executeQuery( getHardcodedLiteral );
			if ( answerBag != null && answerBag.next() )
			{
				int indOfOneIndexedColumns = 1;
				System.out.println(
						here + "got value, " + answerBag.getInt( indOfOneIndexedColumns ) );
			}
			else
			{
				System.err.println( here + "messed something up" );
			}
			answerBag.close();
			incantation.close();
		}
		catch ( java.sql.SQLException se )
		{
			System.err.println( here + "sql fail because " + se );
		}
	}


	public void sketchDaoConnection()
	{
		Properties dbInfo = getConfigFrom( dbInfoFilePath );
		Pool dbThreads = Pool.getInstance();
		dbThreads.adoptDbConnectionOf( dbInfo );

		H2Board marshallsBoards = (H2Board)dbThreads.getMarshaller( TableType.BOARD );
		marshallsBoards.createIfMissing();
		Board original = new Board();
		int priority = 4;
		original.setDesc( cl );
		original.setPriority( priority );
		original = marshallsBoards.save( original, ExceptionFallback.NONE );
		if ( original.getId() < 0 )
			System.err.println( "board didn't save" );
		Map<Board.Field, Object> whereClause = new TreeMap<>();
		whereClause.put( Board.Field.DESC, cl );
		whereClause.put( Board.Field.PRIORITY, priority );
		Board retrieved = marshallsBoards.ofQualities( whereClause, new ArrayList<>() );
		System.out.println( "board retrieved is board saved ? "
				+ (original.getDesc().equals( retrieved.getDesc() )) );

		H2Lane marshallsLanes = (H2Lane) dbThreads.getMarshaller( TableType.LANE );
		marshallsLanes.createIfMissing();
		System.out.println( marshallsLanes.countOf() );
		Lane swim = (Lane) marshallsLanes.ofId( 1 );
		System.out.println( "lane 1 is of board " + swim.getBoard() );
		// NOTE cleanup
		for ( Board currBoard : marshallsBoards.all() )
		{
			marshallsBoards.delete( currBoard );
		}
		dbThreads.closeConnection();
	}


	public void sketchRamInteraction()
	{
		RamStore notDb = RamStore.singleton;
		Scanner input = new Scanner( System.in );
		int boardCount = notDb.countOf( TableType.BOARD );
		if ( boardCount < 1 )
		{
			System.out.print( "no boards exist, create y/n -- " );
			String yn = input.nextLine();
			if ( yn.equals( "y" ) )
			{
				System.out.print( "describe board -- " );
				String boardDesc = input.nextLine();
				Board onlyBoard = new Board();
				onlyBoard.setId( 0 );
				onlyBoard.setDesc( boardDesc );
				onlyBoard.setPriority( 0 );
				onlyBoard.setWhenCreated( LocalDate.now() );
				notDb.save( TableType.BOARD, onlyBoard );
			}
			// else close or whatever
		}
		Board onlyBoard = (Board) notDb.ofId( TableType.BOARD, 0 );
		List<Integer> laneIds = notDb.allIdsOf( TableType.LANE );
		List<Lane> lanesOfOnlyBoard = new LinkedList<>();
		for ( Integer currId : laneIds )
		{
			Lane currLane = (Lane) notDb.ofId( TableType.LANE, currId );
			if ( currLane.getBoard() == onlyBoard.getId() )
			{
				lanesOfOnlyBoard.add( currLane );
			}
		}
		if ( lanesOfOnlyBoard.isEmpty() )
		{
			String yn = "y";
			int laneId = 0;
			while ( yn.equals( "y" ) )
			{
				Lane currLane = new Lane();
				System.out.print( "lane name -- " );
				currLane.setDesc( input.nextLine() );
				currLane.setBoard( onlyBoard.getId() );
				currLane.setId( laneId );
				laneId++;
				notDb.save( TableType.LANE, currLane );
				lanesOfOnlyBoard.add( currLane );

				System.out.print( "add lane to board y/n -- " );
				yn = input.nextLine();
			}
		}
		System.out.println( "board name -- " + onlyBoard.getDesc() );
		System.out.println( "(lanes)" );
		for ( Lane currLane : lanesOfOnlyBoard )
		{
			System.out.print( '\t' + currLane.getDesc() );
		}
		System.out.println( "\n(tasks)" );
		for ( Lane currLane : lanesOfOnlyBoard )
		{
			System.out.print( "\tempty "+ currLane.getId() );
		}
		System.out.println( "\n\ndemo over" );
	}


	public void sketchBoardCreation()
	{
		final String here = cl +"sbc ";
		Properties dbInfo = getConfigFrom( dbInfoFilePath );
		Pool dbThreads = Pool.getInstance();
		dbThreads.adoptDbConnectionOf( dbInfo );

		H2Board marshallsBoards = (H2Board)dbThreads.getMarshaller( TableType.BOARD );
		marshallsBoards.createIfMissing();
		Controller hearsCries = new Controller();
		ObjectMapper reflection = new ObjectMapper();
		reflection.registerModule( new JavaTimeModule() );
		Message newBoard = new Message();
		String sending;
		newBoard.setType( MessageType.RQ_CREATE_BOARD );
		Board original = new Board();
		LocalDate today = LocalDate.now();
		int priority = 1;
		for ( int ind = 0; ind < 3; ind++ )
		{
			original.setDesc( here + ind );
			original.setPriority( priority );
			original.setWhenCreated( today );
			newBoard.setContent( original );
			try
			{
				sending = reflection.writeValueAsString( newBoard );
				System.out.println( sending );
				System.out.println( hearsCries.respond( sending ) );
			}
			catch ( JsonProcessingException jpe )
			{
				jpe.printStackTrace();
			}
		}
		for ( Board currBoard : marshallsBoards.all() )
		{
			marshallsBoards.delete( currBoard );
		}
		dbThreads.closeConnection();
		/*
		Added too many or had some left over from before
	{"type":"RS_CHANGED_BOARD_LIST","version":0.0,"content":[
	{"id":119,"desc":"fb.sbc 0","priority":3,"whenCreated":[2018,9,19],"shared":false},
	{"id":120,"desc":"fb.sbc 1","priority":3,"whenCreated":[2018,9,19],"shared":false},
	{"id":121,"desc":"fb.sbc 1","priority":2,"whenCreated":[2018,9,19],"shared":false},
	{"id":122,"desc":"fb.sbc 2","priority":2,"whenCreated":[2018,9,19],"shared":false},
	{"id":122,"desc":"fb.sbc 2","priority":1,"whenCreated":[2018,9,19],"shared":false}
	]}
		*/
	}


	/** populate a properties file or return empty */
	private Properties getConfigFrom( String resourceFileName )
	{
		Properties resourceMap = new Properties();
		if ( resourceFileName == null )
		{
			return resourceMap;
		}
		try (FileReader ioIn = new FileReader( resourceFileName ))
		{
			resourceMap.load( ioIn );
			if ( resourceMap.size() < 1 )
			{
				System.err.println( cl + "No resources in " + resourceFileName );
			}
		}
		catch ( FileNotFoundException e )
		{
			System.err.println( cl + "Couldn't find " + resourceFileName );
		}
		catch ( IOException e )
		{
			System.err.println( cl + "Couldn't read " + resourceFileName );
		}
		return resourceMap;
	}

}















































