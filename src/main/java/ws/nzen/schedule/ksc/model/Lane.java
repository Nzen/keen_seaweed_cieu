package ws.nzen.schedule.ksc.model;

import java.sql.Date;
import java.time.LocalDate;

/** a container for tasks within a board */
public class Lane extends KscTable
{

	private static final int descLength = 90;

	private int id;
	private int board;
	private String desc;
	private int priority;
	private LocalDate whenCreated;
	private boolean hidden;


	public int getId()
	{
		return id;
	}


	public void setId( int id )
	{
		this.id = id;
	}


	public int getBoard()
	{
		return board;
	}


	public void setBoard( int board )
	{
		this.board = board;
	}


	public String getDesc()
	{
		return desc;
	}


	public void setDesc( String desc )
	{
		if ( desc == null )
		{
			this.desc = "";
		}
		else if ( desc.length() > descLength )
		{
			this.desc = desc.substring( 0, descLength - 1 );
		}
		else
		{
			this.desc = desc;
		}
	}


	public int getPriority()
	{
		return priority;
	}


	public void setPriority( int priority )
	{
		this.priority = priority;
	}


	public LocalDate getWhenCreated()
	{
		return whenCreated;
	}


	public void setWhenCreated( LocalDate when_created )
	{
		this.whenCreated = when_created;
	}


	public void setWhenCreated( Date asSqlD )
	{
		if ( asSqlD == null )
		{
			whenCreated = null;
		}
		else
		{
			whenCreated = asSqlD.toLocalDate();
		}
	}


	public boolean isHidden()
	{
		return hidden;
	}


	public void setHidden( boolean hidden )
	{
		this.hidden = hidden;
	}

}
