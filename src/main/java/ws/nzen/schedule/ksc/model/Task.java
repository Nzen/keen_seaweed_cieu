package ws.nzen.schedule.ksc.model;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class Task extends KscTable
{

	private static final int descLength = 90;
	private static final int utcLength = 6;

	private int id;
	private int board;
	private int lane;
	private String desc;
	private int priority;
	private LocalDateTime whenCreated;
	private String createdUtcOffset; // ex -07:00 ;; +05:30


	public int getId()
	{
		return id;
	}


	public void setId( int id )
	{
		this.id = id;
	}


	public int getBoard()
	{
		return board;
	}


	public void setBoard( int board )
	{
		this.board = board;
	}


	public int getLane()
	{
		return lane;
	}


	public void setLane( int lane )
	{
		this.lane = lane;
	}


	public String getDesc()
	{
		return desc;
	}


	public void setDesc( String desc )
	{
		if ( desc == null )
		{
			this.desc = "";
		}
		else if ( desc.length() > descLength )
		{
			this.desc = desc.substring( 0, descLength - 1 );
		}
		else
		{
			this.desc = desc;
		}
	}


	public int getPriority()
	{
		return priority;
	}


	public void setPriority( int priority )
	{
		this.priority = priority;
	}


	public LocalDateTime getWhenCreated()
	{
		return whenCreated;
	}


	public void setWhenCreated( LocalDateTime whenCreated )
	{
		this.whenCreated = whenCreated;
	}


	public void setWhenCreated( Timestamp asSqlD )
	{
		if ( asSqlD == null )
		{
			whenCreated = null;
		}
		else
		{
			whenCreated = asSqlD.toLocalDateTime();
		}
	}


	public String getCreatedUtcOffset()
	{
		return createdUtcOffset;
	}


	// IMPROVE do some format checking here
	public void setCreatedUtcOffset( String createdUtcOffset )
	{
		this.createdUtcOffset = createdUtcOffset;
	}

}
