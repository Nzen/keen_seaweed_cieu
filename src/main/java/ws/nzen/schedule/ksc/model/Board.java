/**
 * 
 */
package ws.nzen.schedule.ksc.model;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Set;
import java.util.TreeSet;

/** @author nzen */
public class Board extends KscTable
{
	public enum Field
	{
		ID, DESC, PRIORITY, WHEN_CREATED
	};

	private static final int descLength = 90;

	private int id;
	private String desc;
	private int priority;
	private LocalDate whenCreated;
	private transient Set<Field> changed = new TreeSet<>();


	public Set<Field> changes()
	{
		// NOTE paranoia would demand a defensive copy
		return changed;
	}


	public void forgetChanges()
	{
		changed.clear();
	}


	public String toString()
	{
		return "b_b i="+ id +" d~"+ desc
					.substring( 0, Math.min( 10, desc.length() ) )
				+" p="+ priority +" c="+ whenCreated;
	}


	public int getId()
	{
		return id;
	}


	public void setId( int id )
	{
		if ( id != this.id )
		{
			changed.add( Field.ID );
		}
		this.id = id;
	}


	public String getDesc()
	{
		return desc;
	}


	public void setDesc( String desc )
	{
		if ( desc == null )
		{
			this.desc = "";
		}
		else if ( desc.length() > descLength )
		{
			this.desc = desc.substring( 0, descLength - 1 );
		}
		else
		{
			if ( ! desc.equals( this.desc ) )
			{
				changed.add( Field.DESC );
			}
			this.desc = desc;
		}
	}


	public int getPriority()
	{
		return priority;
	}


	public void setPriority( int priority )
	{
		if ( priority != this.priority )
		{
			changed.add( Field.PRIORITY );
		}
		this.priority = priority;
	}


	public LocalDate getWhenCreated()
	{
		return whenCreated;
	}


	public void setWhenCreated( LocalDate whenCreated )
	{
		if ( ! whenCreated.equals( whenCreated ) ) 
		{
			changed.add( Field.WHEN_CREATED );
		}
		this.whenCreated = whenCreated;
	}


	public void sWhenCreated( Date asSqlD )
	{
		if ( asSqlD == null )
		{
			whenCreated = null;
		}
		else
		{
			setWhenCreated( asSqlD.toLocalDate() );
		}
	}

}
