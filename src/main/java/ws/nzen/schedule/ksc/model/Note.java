package ws.nzen.schedule.ksc.model;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class Note extends KscTable
{

	private static final int descLength = 90;
	private static final int utcLength = 6;

	private int id;
	private int task;
	private String desc;
	private LocalDateTime whenCreated;
	private String createdUtcOffset; // ex -07:00 ;; +05:30
	private LocalDateTime whenLastEdited;
	private String editedUtcOffset;


	public int getId()
	{
		return id;
	}


	public void setId( int id )
	{
		this.id = id;
	}


	public int getTask()
	{
		return task;
	}


	public void setTask( int task )
	{
		this.task = task;
	}


	public String getDesc()
	{
		return desc;
	}


	public void setDesc( String desc )
	{
		if ( desc == null )
		{
			this.desc = "";
		}
		else if ( desc.length() > descLength )
		{
			this.desc = desc.substring( 0, descLength - 1 );
		}
		else
		{
			this.desc = desc;
		}
	}


	public LocalDateTime getWhenCreated()
	{
		return whenCreated;
	}


	public void setWhenCreated( LocalDateTime whenCreated )
	{
		this.whenCreated = whenCreated;
	}


	public void setWhenCreated( Timestamp asSqlD )
	{
		if ( asSqlD == null )
		{
			whenCreated = null;
		}
		else
		{
			whenCreated = asSqlD.toLocalDateTime();
		}
	}


	public String getCreatedUtcOffset()
	{
		return createdUtcOffset;
	}


	// IMPROVE do some format checking here
	public void setCreatedUtcOffset( String createdUtcOffset )
	{
		this.createdUtcOffset = createdUtcOffset;
	}


	public LocalDateTime getWhenLastEdited()
	{
		return whenLastEdited;
	}


	public void setWhenLastEdited( LocalDateTime whenLastEdited )
	{
		this.whenLastEdited = whenLastEdited;
	}


	public void setWhenLastEdited( Timestamp asSqlD )
	{
		if ( asSqlD == null )
		{
			whenLastEdited = null;
		}
		else
		{
			whenLastEdited = asSqlD.toLocalDateTime();
		}
	}


	public String getEditedUtcOffset()
	{
		return editedUtcOffset;
	}


	// IMPROVE do some format checking here
	public void setEditedUtcOffset( String editedUtcOffset )
	{
		this.editedUtcOffset = editedUtcOffset;
	}

}
