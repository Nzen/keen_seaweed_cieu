package ws.nzen.schedule.ksc.model;

public enum TableType
{
	BOARD,
	LANE,
	TASK,
	NOTE,
	UNKNOWN;
}
