/** see ../../../../../LICENSE for release details */
package ws.nzen.schedule.ksc.io;

/**  */
public enum MessageType
{
	RQ_LIST_BOARDS, RQ_CREATE_BOARD, RQ_UPDATE_BOARD,
	RQ_DELETE_BOARD, RQ_SEARCH_BOARD,
	RS_CHANGED_BOARD_LIST,
	NOT_UNDERSTOOD, INTERNAL_ERROR, UNKNOWN;

	public static MessageType fromStr( String desired )
	{
		if ( desired == null || desired.isEmpty() )
		{
			return UNKNOWN;
		}
		else
		{
			// IMPROVe perhaps be more flexible by uppercasing the word
			for ( MessageType candidate : values() )
			{
				if ( candidate.name().equals( desired ) )
				{
					return candidate;
				}
			}
			return UNKNOWN;
		}
	}
}


















