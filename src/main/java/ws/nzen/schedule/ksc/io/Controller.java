/** see ../../../../../LICENSE for release details */
package ws.nzen.schedule.ksc.io;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import ws.nzen.schedule.ksc.model.Board;
import ws.nzen.schedule.ksc.operation.CreatesBoard;

/** Adapts between json messages and operations */
public class Controller
{
	private static final String cl = "i.c.";


	public String respond( String jsonRequest )
	{
		final String here = cl +"r ";
		ObjectMapper reflection = new ObjectMapper();
		reflection.registerModule( new JavaTimeModule() );
		if ( jsonRequest == null || jsonRequest.isEmpty() )
		{
			return replyToEmptyRequest( reflection );
		}
		Message said = null;;
		try
		{
			said = reflection.readValue( jsonRequest, Message.class );
		}
		catch ( JsonParseException | JsonMappingException jme )
		{
			System.err.println( here +"unable to parse request "+ jme );
		}
		catch ( IOException ie )
		{
			System.err.println( here +"unable to read request "+ ie );
			// more of a 'if I provided a file' kind of problem
		}
		if ( said == null )
		{
			return replyToEmptyRequest( reflection ); // not actually empty
		}
		else
		{
			JsonNode genericForm = null;
			try
			{
				genericForm = reflection.readTree( jsonRequest );
			}
			catch ( IOException ie )
			{
				System.err.println( here +"unable to read request as tree "+ ie );
				return replyToEmptyRequest( reflection ); // not actually empty
			}
			Message response = provideResponse( said, reflection, genericForm );
			try
			{
				return reflection.writeValueAsString( response );
			}
			catch ( JsonProcessingException jpe )
			{
				System.err.println( here +"unable to marshall response "+ jpe );
				return replyAfterOwnProblem( reflection );
			}
		}
	}


	private String replyToEmptyRequest( ObjectMapper reflection )
	{
		final String here = cl +"rter ";
		Message returnToSender = new Message();
		returnToSender.setType( MessageType.NOT_UNDERSTOOD );
		returnToSender.setVersion( 0F );
		returnToSender.setContent( "Empty request" );
		try
		{
			return reflection.writeValueAsString( returnToSender );
		}
		catch ( JsonProcessingException jpe )
		{
			System.err.println( here +"unable to marshall empty response "+ jpe );
			return "{ \"type\" : \"NOT_UNDERSTOOD\" }"; // oh well
		}
	}


	private String replyAfterOwnProblem( ObjectMapper reflection )
	{
		final String here = cl +"raop ";
		Message returnToSender = new Message();
		returnToSender.setType( MessageType.INTERNAL_ERROR );
		returnToSender.setVersion( 0F );
		returnToSender.setContent( "Couldn't formulate reply" );
		try
		{
			return reflection.writeValueAsString( returnToSender );
		}
		catch ( JsonProcessingException jpe )
		{
			System.err.println( here +"unable to marshall passive response "+ jpe );
			return "{ \"type\" : \"INTERNAL_ERROR\" }";
		}
	}


	private Message provideResponse( Message request,
			ObjectMapper reflection, JsonNode treeForm )
	{
		final String here = cl +"pr ";
		switch ( request.getType() )
		{
			case RQ_CREATE_BOARD :
			{
				Board candidate;
				try
				{
					candidate = reflection.readValue(
							reflection.writeValueAsString(
									treeForm.get( Message.CONTENT_KEY ) ),
							Board.class );
				}
				catch ( IOException jpe )
				{
					System.err.println( here +"unable to read request as tree "+ jpe );
					request.setType( MessageType.NOT_UNDERSTOOD );
					request.setContent( "Couldn't deserialize new board" );
					return request;
				}
				return CreatesBoard.fromMessage( candidate );
			}
			default :
			{
				return request;
			}
		}
	}

}





































