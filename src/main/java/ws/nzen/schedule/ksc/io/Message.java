package ws.nzen.schedule.ksc.io;

public class Message
{
	public static final String CONTENT_KEY = "content";
	private MessageType type;
	private float version = 0.0f;
	private Object content;


	public MessageType getType()
	{
		return type;
	}
	public void setType( MessageType which )
	{
		this.type = which;
	}

	public float getVersion()
	{
		return version;
	}
	public void setVersion( float version )
	{
		this.version = version;
	}

	public Object getContent()
	{
		return content;
	}
	public void setContent( Object content )
	{
		this.content = content;
	}

	
}


















