/** see ../../../../../LICENSE for release details */
package ws.nzen.schedule.ksc.operation;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import ws.nzen.schedule.ksc.dao.ExceptionFallback;
import ws.nzen.schedule.ksc.dao.H2Board;
import ws.nzen.schedule.ksc.dao.Pool;
import ws.nzen.schedule.ksc.io.Message;
import ws.nzen.schedule.ksc.io.MessageType;
import ws.nzen.schedule.ksc.model.Board;
import ws.nzen.schedule.ksc.model.TableType;

/**  */
public class CreatesBoard
{
	public static Message fromMessage( Board desired )
	{
		// assert desired != null
		Pool dbThreads = Pool.getInstance();
		H2Board marshallsBoards = (H2Board)dbThreads
				.getMarshaller( TableType.BOARD );
		desired = marshallsBoards.save( desired, ExceptionFallback.NONE );
		// IMPROVE if () desired is invalid id, say internal error
		Map<Board.Field, Object> whereClause = new TreeMap<>();
		whereClause.put( Board.Field.PRIORITY, desired.getPriority() );
		if ( marshallsBoards.countOf( whereClause ) > 1 )
		{
			// reorganize the other boards so it has the right priority
			return ReorganizesBoards.forBoard( desired, marshallsBoards, true );
		}
		else
		{
			List<Board> atTheEnd = new LinkedList<>();
			atTheEnd.add( desired );
			Message changedBoards = new Message();
			changedBoards.setType( MessageType.RS_CHANGED_BOARD_LIST );
			changedBoards.setContent( atTheEnd );
			return changedBoards;
		}
	}

}


















