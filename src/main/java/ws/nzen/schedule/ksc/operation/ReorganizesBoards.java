/** see ../../../../../LICENSE for release details */
package ws.nzen.schedule.ksc.operation;

import java.util.LinkedList;
import java.util.List;

import ws.nzen.schedule.ksc.dao.ExceptionFallback;
import ws.nzen.schedule.ksc.dao.H2Board;
import ws.nzen.schedule.ksc.io.Message;
import ws.nzen.schedule.ksc.io.MessageType;
import ws.nzen.schedule.ksc.model.Board;

/**  */
public class ReorganizesBoards
{

	public static Message forBoard( Board changedPriority,
			H2Board marshallsBoards, boolean creatingNotUpdating )
	{
		List<Board> changed = new LinkedList<>();
		List<Board> allBoards = marshallsBoards.all();
		if ( creatingNotUpdating )
		{
			for ( Board curr : allBoards )
			{
				if ( curr.getPriority() >= changedPriority.getPriority() )
				{
					// assert priorities are monotonically increasing integers
					curr.setPriority( curr.getPriority() +1 );
					changed.add( curr );
				}
			}
			marshallsBoards.save( changedPriority, ExceptionFallback.NONE );
		}
		else
		{
			// NOTE finding the right range
			int formerPriority = -1, formerInd = 0;
			for ( Board candidate : allBoards )
			{
				if ( candidate.getId() == changedPriority.getId() )
				{
					formerPriority = candidate.getPriority();
					break;
				}
				else
				{
					formerInd++;
				}
			}
			allBoards.remove( formerInd );
			int lowEnd = Math.min( formerPriority, changedPriority.getPriority() );
			int highEnd = Math.min( formerPriority, changedPriority.getPriority() );
			for ( Board curr : allBoards )
			{
				if ( curr.getPriority() >= lowEnd && curr.getPriority() <= highEnd )
				{
					// assert priorities are monotonically increasing integers
					curr.setPriority( curr.getPriority() +1 );
					changed.add( curr );
				}
			}
			changed.add( changedPriority );
		}
		// IMPROVe use a transaction
		for ( Board toSet : changed )
		{
			marshallsBoards.save( toSet );
		}
		if ( creatingNotUpdating )
		{
			changed.add( changedPriority );
		}
		Message changedBoards = new Message();
		changedBoards.setType( MessageType.RS_CHANGED_BOARD_LIST );
		changedBoards.setContent( changed );
		return changedBoards;
	}

}




























































