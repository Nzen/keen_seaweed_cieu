/**
 * 
 */
package ws.nzen.schedule.ksc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import ws.nzen.schedule.ksc.model.KscTable;

/** @author nzen */
public abstract class CoreDao
{

	protected static final int firstRsInd = 1;
	private static final String cl = "cd.";
	protected final String tableName;
	protected Connection dbPipe;


	protected CoreDao( Connection connector, String tableN )
	{
		dbPipe = connector;
		tableName = tableN;
		// Improve throw npe if tableN is blank
	}


	public abstract boolean createIfMissing();


	protected boolean createIfMissing(
			String vettingQuery, String ddlForTable )
	{
		final String here = cl + "oi ";
		final boolean worked = true;
		try (PreparedStatement strictRequest = dbPipe.prepareStatement( vettingQuery );)
		{
			ResultSet answers = strictRequest.executeQuery();
			if ( answers.next() )
			{
				Integer tempI = answers.getInt( firstRsInd );
				return worked;
			}
		}
		catch ( SQLException se )
		{
			try (PreparedStatement strictRequest = dbPipe.prepareStatement( ddlForTable );)
			{
				int rows = strictRequest.executeUpdate();
				System.err.println( here + "created " + tableName + " because " + rows );
				return worked;
			}
			catch ( SQLException cse )
			{
				System.err.println( here + "couldn't create " + tableName + " because " + cse );
				String showAllTables = "SELECT\ntable_name\nFROM\n" +
						"INFORMATION_SCHEMA.TABLES\nWHERE\ntable_schema = 'PUBLIC'";
				try (PreparedStatement strictRequest = dbPipe.prepareStatement( showAllTables );)
				{
					ResultSet answers = strictRequest.executeQuery();
					boolean didOne = false;
					while ( answers.next() )
					{
						System.out.println( here + answers.getString( firstRsInd )
								+ " is an existing table" );
						didOne |= true;
					}
					if ( ! didOne )
					{
						System.out.println( here + "no tables officially exist" );
					}
				}
				catch ( SQLException isse )
				{
					System.err.println( here + "couldn't list all tables because " + isse );
				}
			}
			return ! worked;
		}
		return worked;
	}


	/** how many records exist or -1 if something went wrong */
	public int countOf()
	{
		int invalid = - 1;
		String countOf = "SELECT COUNT( 1 ) AS co FROM " + tableName;
		try (PreparedStatement strictCounter = dbPipe.prepareStatement( countOf );
				ResultSet answers = strictCounter.executeQuery())
		{
			if ( answers != null && answers.next() )
			{
				return answers.getInt( firstRsInd );
			}
			else
			{
				return invalid;
			}
		}
		catch ( SQLException se )
		{
			System.err.println( cl + "co couldn't count "
					+ tableName + "s because " + se );
			return invalid;
		}
	}

}
