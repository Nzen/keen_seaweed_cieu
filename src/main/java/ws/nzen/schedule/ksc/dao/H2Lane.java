package ws.nzen.schedule.ksc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import ws.nzen.schedule.ksc.model.Lane;

public class H2Lane extends CoreDao
{

	private static final String cl = "hl.";
	private static final String dbTable = "BASE_LANE";
	private static final String snId = "id";
	private static final String snBoard = "board";
	private static final String snDesc = "desc";
	private static final String snPriority = "priority";
	private static final String snHidden = "hidden";


	public H2Lane( Connection dbPipe )
	{
		super( dbPipe, dbTable );
	}


	@Override
	public boolean createIfMissing()
	{
		return createIfMissing( String.format( "SELECT %s FROM %s", snId, tableName ),
				"CREATE TABLE PUBLIC.base_lane (id INT AUTO_INCREMENT NOT NULL,"
						+ " board INT, desc VARCHAR(90), priority INT,"
						+ " when_created date DEFAULT CURRENT_DATE(), hidden BOOLEAN DEFAULT FALSE,"
						+ " CONSTRAINT PK_BASE_LANE PRIMARY KEY (id),"
						+ " CONSTRAINT bl_fk_board FOREIGN KEY (board) REFERENCES PUBLIC.base_board( id ));" );
	}


	public Lane ofId( int laneId )
	{
		final String here = cl + "oi ";
		Lane chosen = new Lane();
		// ASK handle versioning ? at work we
		final int boardInd = firstRsInd, descInd = boardInd + 1,
				prioInd = descInd + 1, hidInd = prioInd + 1;
		String laneOfIdQ = String.format( "SELECT %s, %s, %s, %s FROM %s WHERE %s = ?",
				snBoard, snDesc, snPriority, snHidden, tableName, snId );
		try (PreparedStatement strictRequest = dbPipe.prepareStatement( laneOfIdQ );)
		{
			strictRequest.setInt( firstRsInd, laneId ); // NOTE filling the statement
			ResultSet answers = strictRequest.executeQuery();
			if ( answers.next() )
			{
				Integer tempI = answers.getInt( boardInd );
				if ( tempI == null )
				{
					// NOTE invalid record, because this is a foreign key
					chosen = withMinBlanks( chosen );
				}
				else
				{
					chosen.setId( laneId );
					chosen.setBoard( answers.getInt( boardInd ) );
					chosen.setDesc( answers.getString( descInd ) );
					chosen.setPriority( answers.getInt( prioInd ) );
					chosen.setHidden( answers.getBoolean( hidInd ) );
				}
			}
			else
			{
				chosen = withMinBlanks( chosen );
			}
			answers.close();
		}
		catch ( SQLException se )
		{
			System.err.println( here + "couldn't get lane " + laneId + " because " + se );
			chosen = withMinBlanks( chosen );
		}
		return chosen;
	}


	private Lane withMinBlanks( Lane empty )
	{
		final int invalid = - 2_000;
		empty.setId( invalid );
		empty.setBoard( invalid );
		empty.setDesc( "" );
		empty.setPriority( invalid );
		empty.setHidden( true );
		return empty;
	}

}
