package ws.nzen.schedule.ksc.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jooq.Configuration;
import org.jooq.Record;
import org.jooq.SQLDialect;
import org.jooq.UpdateSetMoreStep;
import org.jooq.UpdateSetStep;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultConfiguration;

import ws.nzen.schedule.ksc.model.Board;

public class H2Board extends CoreDao
{

	private static final String cl = "hb.";
	private static final String dbTable = "BASE_BOARD";
	private static final String snId = "id";
	private static final String snDesc = "desc";
	private static final String snWhenCreated = "when_created";
	private static final String snPriority = "priority";
	final int invalid = - 2_000;


	public H2Board( Connection dbPipe )
	{
		super( dbPipe, dbTable );
	}


	@Override
	public boolean createIfMissing()
	{
		return createIfMissing( String.format( "SELECT %s FROM %s", snId, tableName ),
				"CREATE TABLE PUBLIC.base_board (id INT AUTO_INCREMENT NOT NULL,"
						+ " desc VARCHAR(90), priority INT, when_created date DEFAULT CURRENT_DATE(),"
						+ " CONSTRAINT PK_BASE_BOARD PRIMARY KEY (id));" );
	}


	public Board ofId( int boardId )
	{
		final String here = cl + "oi ";
		Board chosen = new Board();
		final int descInd = firstRsInd, creaInd = descInd + 1,
				prInd = creaInd + 1, sharInd = prInd + 1;
		String boardOfIdQ = String.format( "SELECT %s, %s, %s"
				+ " FROM %s WHERE %s = ?", snDesc, snWhenCreated,
				snPriority, tableName, snId );
		try (PreparedStatement strictRequest = dbPipe.prepareStatement( boardOfIdQ );)
		{
			strictRequest.setInt( firstRsInd, boardId );
			ResultSet answers = strictRequest.executeQuery();
			if ( answers != null && answers.next() )
			{
				String tempI = answers.getString( descInd );
				if ( tempI == null )
				{
					chosen = withMinBlanks( chosen );
				}
				else
				{
					chosen.setId( boardId );
					chosen.setDesc( answers.getString( descInd ) );
					chosen.sWhenCreated( answers.getDate( creaInd ) );
					chosen.setPriority( answers.getInt( prInd ) );
				}
			}
			else
			{
				chosen = withMinBlanks( chosen );
			}
			answers.close();
		}
		catch ( SQLException se )
		{
			System.err.println( here + "couldn't get board " + boardId
					+ " because " + se );
			chosen = withMinBlanks( chosen );
		}
		return chosen;
	}


	public Board ofQualities( Map<Board.Field, Object> qualities,
			List<Board.Field> orderBy )
	{
		final String here = cl + "oq ";
		Board chosen = new Board();
		if ( qualities == null || qualities.isEmpty() )
		{
			chosen.setId( invalid );
			return chosen;
		}
		final int idInd = firstRsInd, descInd = idInd +1,
				creaInd = descInd + 1, prInd = creaInd + 1,
				sharInd = prInd + 1;
		String boardQ = String.format( "SELECT %s, %s, %s, %s"
				+ " FROM %s WHERE 1=1", snId, snDesc,
				snWhenCreated, snPriority, tableName );
		StringBuilder fullQ = new StringBuilder( boardQ );
		for ( Board.Field currField : qualities.keySet() )
		{
			fullQ.append( " AND " ); // NOTE simplified because of true above
			fullQ.append( mapFieldToSqlName( currField ) );
			fullQ.append( " = ?" );
		}
		if ( ! orderBy.isEmpty() )
		{
			fullQ.append( " ORDER BY " );
			for ( Board.Field currField : orderBy )
			{
				fullQ.append( mapFieldToSqlName( currField ) );
				fullQ.append( ", " );
			}
			fullQ.deleteCharAt( fullQ.length() -2 ); // NOTE trailing comma
		}
		try (PreparedStatement strictRequest = dbPipe
				.prepareStatement( fullQ.toString() );)
		{
			int ind = firstRsInd;
			for ( Board.Field currField : qualities.keySet() )
			{
				setInPreparedStatement( currField, qualities
						.get( currField ), strictRequest, ind );
				ind++;
			}
			ResultSet answers = strictRequest.executeQuery();
			if ( answers != null && answers.next() )
			{
				String tempI = answers.getString( descInd );
				if ( tempI == null )
				{
					chosen = withMinBlanks( chosen );
				}
				else
				{
					chosen.setId( answers.getInt( idInd ) );
					chosen.setDesc( answers.getString( descInd ) );
					chosen.sWhenCreated( answers.getDate( creaInd ) );
					chosen.setPriority( answers.getInt( prInd ) );
				}
			}
			else
			{
				chosen = withMinBlanks( chosen );
			}
			answers.close();
		}
		catch ( SQLException se )
		{
			System.err.println( here + "couldn't get board"
					+ " because " + se );
			chosen = withMinBlanks( chosen );
		}
		return chosen;
	}


	public List<Board> all()
	{
		final String here = cl + "a ";
		List<Board> found = new ArrayList<>();
		// NOTE ignoring optimization of marshalling them here, to avoid repetition
		List<Integer> idsOfAll = new ArrayList<>();
		String allBoardQ = String.format( "SELECT %s FROM %s", snId, tableName );
		try (PreparedStatement strictRequest = dbPipe.prepareStatement( allBoardQ );)
		{
			ResultSet answers = strictRequest.executeQuery();
			while ( answers.next() )
			{
				idsOfAll.add( answers.getInt( firstRsInd ) );
			}
			answers.close();
		}
		catch ( SQLException se )
		{
			System.err.println( here + "couldn't get boards because " + se );
		}
		for ( Integer currId : idsOfAll )
		{
			found.add( ofId( currId ) );
		}
		return found;
	}


	/** insert record */
	public Board save( Board freshest, ExceptionFallback fallback )
	{
		final String here = cl + "s ";
		if ( freshest.getWhenCreated() == null )
		{
			freshest.setWhenCreated( LocalDate.now() );
		}
		final int descInd = firstRsInd, creaInd = descInd + 1,
				prInd = creaInd + 1, sharInd = prInd + 1;
		String insertBoard = String.format( "INSERT INTO %s"
				+ " ( %s, %s, %s, %s ) VALUES ( ?, ?, ? )",
				tableName, snDesc, snWhenCreated, snPriority );
		try (PreparedStatement strictRequest = dbPipe.prepareStatement( insertBoard );)
		{
			strictRequest.setString( descInd, freshest.getDesc() );
			strictRequest.setDate( creaInd, Date.valueOf( freshest.getWhenCreated() ) );
			strictRequest.setInt( prInd, freshest.getPriority() );
			int rows = strictRequest.executeUpdate();
		}
		catch ( SQLException se )
		{
			// IMPROVE if se is complaining about the id, consider updating or getting max again
			if ( fallback != ExceptionFallback.NONE )
			{
				System.err.println( here + "couldn't write board "
						+ freshest.getId() +" because " + se );
			}
			freshest.setId( invalid );
		}
		if ( freshest.getId() != invalid )
		{
			Map<Board.Field, Object> matchFreshest = new HashMap<>();
			matchFreshest.put( Board.Field.DESC, freshest.getDesc() );
			matchFreshest.put( Board.Field.PRIORITY, freshest.getPriority() );
			matchFreshest.put( Board.Field.WHEN_CREATED, freshest.getWhenCreated() );
			Board retrieved = ofQualities( matchFreshest, new LinkedList<>() );
			freshest.setId( retrieved.getId() );
			freshest.forgetChanges();
		}
		return freshest;
	}


	/** update record */
	public Board save( Board freshest )
	{
		final String here = cl + "s ";
		String updateBoard = statementToUpdate( freshest );
		if ( updateBoard.isEmpty() )
		{
			return freshest; // NOTE nothing changed
		}
		try (PreparedStatement strictRequest = dbPipe.prepareStatement( updateBoard );)
		{
			// FIX gen what it wants or push this bs into stmToUpdate
			int rows = strictRequest.executeUpdate();
			if ( rows != 1 )
			{
				freshest.setId( invalid );
			}
			else
			{
				freshest.forgetChanges();
			}
		}
		catch ( SQLException se )
		{
			System.err.println( here + "couldn't write board "
					+ freshest.getId() +" because " + se );
			freshest.setId( invalid ); // ASK perhaps too destructive, given I'm not keeping the value
		}
		return freshest;
	}


	private String statementToUpdate( Board freshest )
	{
		Set<Board.Field> changed = freshest.changes();
		if ( changed.isEmpty() )
		{
			// to be fair, someone could have forgotten the changes
			return "";
		}
		Configuration jooqMemory = new DefaultConfiguration().set( SQLDialect.H2 );
		@SuppressWarnings("unchecked")
		UpdateSetMoreStep<Record> changeWhatDid = (UpdateSetMoreStep<Record>) DSL
				.using( jooqMemory )
				.update( DSL.table( tableName ) );
		for ( Board.Field currField : changed )
		{
			switch ( currField )
			{
				case DESC :
				{
					changeWhatDid = changeWhatDid.set( DSL.field( snDesc ), freshest.getDesc() );
					break;
				}
				case WHEN_CREATED :
				{
					changeWhatDid = changeWhatDid.set( DSL.field( snWhenCreated ), freshest.getWhenCreated() );
					break;
				}
				case PRIORITY :
				{
					changeWhatDid = changeWhatDid.set( DSL.field( snPriority ), freshest.getPriority() );
					break;
				}
				default :
				{
					break;
				}
			}
		}
		// below wants DslContext Settings .setStatementType ( STATIC_STATEMENT  ) instead
		return changeWhatDid.where( DSL.field( snId ).eq( freshest.getId() ) ).getSQL( true );
	}


	public boolean delete( Board unwelcome )
	{
		final String here = cl + "oq ";
		final boolean worked = true;
		if ( unwelcome == null || unwelcome.getId() == invalid )
		{
			return worked;
		}
		// ASK check if children exist; throw exception ?
		String boardQ = String.format(
				"DELETE FROM %s WHERE %s = ?", tableName, snId );
		try (PreparedStatement strictRequest = dbPipe
				.prepareStatement( boardQ );)
		{
			strictRequest.setInt( firstRsInd, unwelcome.getId() );
			int rows = strictRequest.executeUpdate();
			return rows == 1;
		}
		catch ( SQLException se )
		{
			System.err.println( here + "couldn't delete board"
					+ " because " + se );
			return ! worked;
		}
	}


	/** how many records exist or -1 if something went wrong;
	 * of exact quality */
	public int countOf(  Map<Board.Field, Object> qualities )
	{
		int invalid = - 1;
		String countOf = "SELECT COUNT( 1 ) AS co FROM "
				+ tableName +" WHERE 1=1";
		StringBuilder fullQ = new StringBuilder( countOf );
		for ( Board.Field currField : qualities.keySet() )
		{
			fullQ.append( " AND " ); // NOTE simplified because of true above
			fullQ.append( mapFieldToSqlName( currField ) );
			fullQ.append( " = ?" );
		}
		try (PreparedStatement strictCounter = dbPipe.prepareStatement( fullQ.toString() );)
		{
			int ind = firstRsInd;
			for ( Board.Field currField : qualities.keySet() )
			{
				setInPreparedStatement( currField, qualities
						.get( currField ), strictCounter, ind );
				ind++;
			}
			ResultSet answers = strictCounter.executeQuery();
			if ( answers.next() )
			{
				int qtyThatMatch = answers.getInt( firstRsInd );
				answers.close();
				return qtyThatMatch;
			}
			answers.close();
		}
		catch ( SQLException se )
		{
			System.err.println( cl + "co couldn't count "
					+ tableName + "s because " + se );
		}
		return invalid;
	}


	private Board withMinBlanks( Board toEmpty )
	{
		toEmpty.setId( invalid );
		toEmpty.setDesc( "" );
		toEmpty.setWhenCreated( LocalDate.now() );
		toEmpty.setPriority( invalid );
		return toEmpty;
	}


	protected void setInPreparedStatement( Board.Field which, Object value,
			PreparedStatement appendTo, int ind ) throws SQLException
	{
		switch ( which )
		{
			case DESC :
			{
				appendTo.setString( ind, (String)value );
				break;
			}
			case WHEN_CREATED :
			{
				appendTo.setDate( ind, Date.valueOf( (LocalDate)value) );
				break;
			}
			case ID :
			case PRIORITY :
			{
				appendTo.setInt( ind, (Integer)value );
				break;
			}
		}
	}


	protected String mapFieldToSqlName( Board.Field which )
	{
		switch ( which )
		{
			case ID :
			{
				return snId;
			}
			case DESC :
			{
				return snDesc;
			}
			case WHEN_CREATED :
			{
				return snWhenCreated;
			}
			case PRIORITY :
			{
				return snPriority;
			}
			default :
			{
				return "";
			}
		}
	}


}






























