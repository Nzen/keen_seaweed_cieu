package ws.nzen.schedule.ksc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

import ws.nzen.schedule.ksc.model.KscTable;
import ws.nzen.schedule.ksc.model.Note;

public class H2Note extends CoreDao
{

	private static final String cl = "hn.";
	private static final String dbTable = "BASE_NOTE";
	private static final String snId = "id";
	private static final String snTask = "task";
	private static final String snDesc = "desc";
	private static final String snWhenCreated = "when_created";
	private static final String snCreatedUtcOffset = "created_utc_offset";
	private static final String snWhenLastEdited = "when_last_edited";
	private static final String snEditedUtcOffset = "edited_utc_offset";


	public H2Note( Connection dbPipe )
	{
		super( dbPipe, dbTable );
	}


	@Override
	public boolean createIfMissing()
	{
		return createIfMissing( String.format( "SELECT %s FROM %s", snId, tableName ),
				"CREATE TABLE PUBLIC.base_note (id INT AUTO_INCREMENT NOT NULL,"
						+ " task INT, desc CLOB, when_created TIMESTAMP DEFAULT NOW(),"
						+ " created_utc_offset CHAR(6), when_last_edited TIMESTAMP,"
						+ " edited_utc_offset CHAR(6), CONSTRAINT PK_BASE_NOTE PRIMARY KEY (id),"
						+ " CONSTRAINT bn_fk_task FOREIGN KEY (task) REFERENCES PUBLIC.base_task( id ));" );
	}


	public Note ofId( int id )
	{
		final String here = cl + "oi ";
		Note chosen = new Note();
		final int taskInd = firstRsInd, descInd = taskInd + 1,
				creInd = descInd + 1, cUtcInd = creInd + 1,
				edInd = cUtcInd + 1, eUtcInd = edInd + 1;
		String laneOfIdQ = String.format( "SELECT %s, %s, %s,"
				+ " %s, %s FROM %s WHERE %s = ?",
				snTask, snDesc, snWhenCreated, snCreatedUtcOffset,
				snWhenLastEdited, snEditedUtcOffset, tableName, snId );
		try (PreparedStatement strictRequest = dbPipe.prepareStatement( laneOfIdQ );)
		{
			strictRequest.setInt( firstRsInd, id );
			ResultSet answers = strictRequest.executeQuery();
			if ( answers.next() )
			{
				chosen.setId( id );
				chosen.setTask( answers.getInt( taskInd ) );
				chosen.setDesc( answers.getString( descInd ) );
				chosen.setWhenCreated( answers.getTimestamp( creInd ) );
				chosen.setCreatedUtcOffset( answers.getString( cUtcInd ) );
				chosen.setWhenLastEdited( answers.getTimestamp( edInd ) );
				chosen.setEditedUtcOffset( answers.getString( eUtcInd ) );
			}
			else
			{
				chosen = withMinBlanks( chosen );
			}
			answers.close();
		}
		catch ( SQLException se )
		{
			System.err.println( here + "couldn't get note " + id + " because " + se );
			chosen = withMinBlanks( chosen );
		}
		return chosen;
	}


	private Note withMinBlanks( Note empty )
	{
		final int invalid = - 2_000;
		final String noOffset = "+00:00";
		empty.setId( invalid );
		empty.setTask( invalid );
		empty.setDesc( "" );
		empty.setWhenCreated( LocalDateTime.now() );
		empty.setCreatedUtcOffset( noOffset );
		empty.setWhenLastEdited( LocalDateTime.now() );
		empty.setEditedUtcOffset( noOffset );
		return empty;
	}

}
