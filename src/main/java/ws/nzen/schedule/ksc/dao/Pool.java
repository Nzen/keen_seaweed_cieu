package ws.nzen.schedule.ksc.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import ws.nzen.schedule.ksc.model.TableType;

public class Pool
{

	private static final String cl = "p.";
	private static String dbUrl = "jdbc:h2:file:~/test/ksc_db",
			dbUser = "sa", dbPassword = "";
	public static final String propKeyUrl = "db_url", propKeyUser = "db_user",
			propKeyPassword = "db_password_unencrypted";
	private Connection dbPipe;


	public static Pool getInstance()
	{
		return OnlyPool.instance;
	}


	private Pool()
	{
		;
	}


	public void adoptDbConnectionOf( Properties hasValues )
	{
		if ( hasValues == null || hasValues.isEmpty() )
		{
			return;
		}
		dbUrl = hasValues.getProperty( propKeyUrl, dbUrl );
		dbUser = hasValues.getProperty( propKeyUser, dbUser );
		dbPassword = hasValues.getProperty( propKeyPassword, dbPassword );
	}


	public void openConnection() throws SQLException
	{
		if ( dbPipe == null || dbPipe.isClosed() )
		{
			dbPipe = java.sql.DriverManager
					.getConnection( dbUrl, dbUser, dbPassword );
			// IMPROVE defer to a connection pool or whatever
		}
	}


	public void closeConnection()
	{
		try
		{
			if ( dbPipe != null && ! dbPipe.isClosed() )
			{
				dbPipe.close();
			}
		}
		catch ( SQLException se )
		{
			// TODO Auto-generated catch block
			se.printStackTrace();
		}
	}


	public CoreDao getMarshaller( TableType which )
	{
		final String here = cl + "gm ";
		try
		{
			if ( dbPipe == null || dbPipe.isClosed() )
			{
				openConnection();
			}
		}
		catch ( SQLException se )
		{
			System.err.println( here + "couldn't open conn " + se );
		}
		switch ( which )
		{
			case LANE :
			{
				return new H2Lane( dbPipe );
			}
			case BOARD :
			{
				return new H2Board( dbPipe );
			}
			default :
			{
				throw new RuntimeException( here + "unimplemented for " + which.name() );
			}
		}
	}

	private static class OnlyPool
	{
		public static final Pool instance;

		static
		{
			Pool livingObj;
			try
			{
				livingObj = new Pool();
			}
			catch ( Exception any )
			{
				livingObj = null;
			}
			instance = livingObj;
		}
	}
}
