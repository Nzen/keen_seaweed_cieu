/*
 * This file is generated by jOOQ.
 */
package ws.nzen.schedule.ksc.dao.jooq.tables;


import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;

import ws.nzen.schedule.ksc.dao.jooq.Indexes;
import ws.nzen.schedule.ksc.dao.jooq.Keys;
import ws.nzen.schedule.ksc.dao.jooq.Public;
import ws.nzen.schedule.ksc.dao.jooq.tables.records.BaseUserRecord;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.4"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class BaseUser extends TableImpl<BaseUserRecord> {

    private static final long serialVersionUID = -895051896;

    /**
     * The reference instance of <code>PUBLIC.BASE_USER</code>
     */
    public static final BaseUser BASE_USER = new BaseUser();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<BaseUserRecord> getRecordType() {
        return BaseUserRecord.class;
    }

    /**
     * The column <code>PUBLIC.BASE_USER.ID</code>.
     */
    public final TableField<BaseUserRecord, Integer> ID = createField("ID", org.jooq.impl.SQLDataType.INTEGER.nullable(false).identity(true), this, "");

    /**
     * The column <code>PUBLIC.BASE_USER.DESC</code>.
     */
    public final TableField<BaseUserRecord, String> DESC = createField("DESC", org.jooq.impl.SQLDataType.VARCHAR(90), this, "");

    /**
     * The column <code>PUBLIC.BASE_USER.WHEN_CREATED</code>.
     */
    public final TableField<BaseUserRecord, LocalDate> WHEN_CREATED = createField("WHEN_CREATED", org.jooq.impl.SQLDataType.LOCALDATE.defaultValue(org.jooq.impl.DSL.field("CURRENT_DATE()", org.jooq.impl.SQLDataType.LOCALDATE)), this, "");

    /**
     * The column <code>PUBLIC.BASE_USER.PASSWORD_HASH</code>.
     */
    public final TableField<BaseUserRecord, String> PASSWORD_HASH = createField("PASSWORD_HASH", org.jooq.impl.SQLDataType.VARCHAR(256), this, "");

    /**
     * Create a <code>PUBLIC.BASE_USER</code> table reference
     */
    public BaseUser() {
        this(DSL.name("BASE_USER"), null);
    }

    /**
     * Create an aliased <code>PUBLIC.BASE_USER</code> table reference
     */
    public BaseUser(String alias) {
        this(DSL.name(alias), BASE_USER);
    }

    /**
     * Create an aliased <code>PUBLIC.BASE_USER</code> table reference
     */
    public BaseUser(Name alias) {
        this(alias, BASE_USER);
    }

    private BaseUser(Name alias, Table<BaseUserRecord> aliased) {
        this(alias, aliased, null);
    }

    private BaseUser(Name alias, Table<BaseUserRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> BaseUser(Table<O> child, ForeignKey<O, BaseUserRecord> key) {
        super(child, key, BASE_USER);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.PRIMARY_KEY_3);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<BaseUserRecord, Integer> getIdentity() {
        return Keys.IDENTITY_BASE_USER;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<BaseUserRecord> getPrimaryKey() {
        return Keys.PK_BASE_USER;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<BaseUserRecord>> getKeys() {
        return Arrays.<UniqueKey<BaseUserRecord>>asList(Keys.PK_BASE_USER);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BaseUser as(String alias) {
        return new BaseUser(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BaseUser as(Name alias) {
        return new BaseUser(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public BaseUser rename(String name) {
        return new BaseUser(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public BaseUser rename(Name name) {
        return new BaseUser(name, null);
    }
}
