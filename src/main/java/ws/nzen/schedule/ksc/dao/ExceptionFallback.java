package ws.nzen.schedule.ksc.dao;

/** requested fallback action in case of exception */
public enum ExceptionFallback
{
	UPDATE_WITH_SAME_ID,
	INSERT_WITH_INCREMENTED_ID,
	NONE;
}


















