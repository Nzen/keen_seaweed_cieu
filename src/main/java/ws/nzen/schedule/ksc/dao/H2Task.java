package ws.nzen.schedule.ksc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

import ws.nzen.schedule.ksc.model.KscTable;
import ws.nzen.schedule.ksc.model.Task;

public class H2Task extends CoreDao
{

	private static final String cl = "ht.";
	private static final String dbTable = "BASE_TASK";
	private static final String snId = "id";
	private static final String snLane = "lane";
	private static final String snDesc = "desc";
	private static final String snPriority = "priority";
	private static final String snWhenCreated = "when_created";
	private static final String snCreatedUtcOffset = "created_utc_offset";


	public H2Task( Connection dbPipe )
	{
		super( dbPipe, dbTable );
	}


	@Override
	public boolean createIfMissing()
	{
		return createIfMissing( String.format( "SELECT %s FROM %s", snId, tableName ),
				"CREATE TABLE PUBLIC.base_task (id INT AUTO_INCREMENT NOT NULL, lane INT,"
						+ " desc VARCHAR(90), priority INT, when_created TIMESTAMP DEFAULT NOW(),"
						+ " created_utc_offset CHAR(6), CONSTRAINT PK_BASE_TASK PRIMARY KEY (id),"
						+ " CONSTRAINT bt_fk_lane FOREIGN KEY (lane) REFERENCES PUBLIC.base_lane( id ));" );
	}


	public Task ofId( int id )
	{
		final String here = cl + "oi ";
		Task chosen = new Task();
		final int laneInd = firstRsInd, descInd = laneInd + 1,
				prioInd = descInd + 1, whenInd = prioInd + 1,
				utcInd = whenInd + 1;
		String laneOfIdQ = String.format( "SELECT %s, %s, %s,"
				+ " %s, %s FROM %s WHERE %s = ?",
				snLane, snDesc, snPriority, snWhenCreated,
				snCreatedUtcOffset, tableName, snId );
		try (PreparedStatement strictRequest = dbPipe.prepareStatement( laneOfIdQ );)
		{
			strictRequest.setInt( firstRsInd, id );
			ResultSet answers = strictRequest.executeQuery();
			if ( answers.next() )
			{
				chosen.setId( id );
				chosen.setLane( answers.getInt( laneInd ) );
				chosen.setDesc( answers.getString( descInd ) );
				chosen.setPriority( answers.getInt( prioInd ) );
				chosen.setWhenCreated( answers.getTimestamp( whenInd ) );
				chosen.setCreatedUtcOffset( answers.getString( utcInd ) );
			}
			else
			{
				chosen = withMinBlanks( chosen );
			}
			answers.close();
		}
		catch ( SQLException se )
		{
			System.err.println( here + "couldn't get task " + id + " because " + se );
			chosen = withMinBlanks( chosen );
		}
		return chosen;
	}


	private Task withMinBlanks( Task empty )
	{
		final int invalid = - 2_000;
		final String noOffset = "+00:00";
		empty.setId( invalid );
		empty.setLane( invalid );
		empty.setDesc( "" );
		empty.setPriority( invalid );
		empty.setWhenCreated( LocalDateTime.now() );
		empty.setCreatedUtcOffset( noOffset );
		return empty;
	}

}
